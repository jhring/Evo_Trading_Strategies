#!/bin/sh 

x=( $(ls | grep ^r\_.*csv) )
xlen=${#x[@]}
rbw="RAINBOW"
vnl="VANILLA"

for ((i=0; i<=${xlen}-1; i++))
do
	matlab -nodesktop -nosplash -r "data_2_prices('${x[$i]}', '$rbw${x[$i]}', '$vnl${x[$i]}'); exit"
done 
