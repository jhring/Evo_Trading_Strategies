function price = rainbow_option_price( settle, maturity, rfr, basis, ...
    assetPrices, vols, divs, strike, corrs, optSpec, maxmin )
    % RAINBOW_OPTION_PRICE
    % Calculates the price of a European rainbow option based on the maximum or
    % minimum of two underlying assets.
    % Asset prices are assumed to follow geometric Brownian motion. 
    % ~~~ PARAMETERS ~~~
    % settle: date on which option is to be traded (usually today's date).
    % Given in the format 'Nov-21-2017'
    % maturity: date on which option can be exercised. This is a European
    % option, so option can only be exercised on one specific date. Given in
    % the format 'Nov-21-2018'
    % rfr: risk-free interest rate, typically rate on three-month T-bill. Given
    % in the format rfr = 0.05
    % basis: day-count basis. Usually set to 1, means that there is a 360 day
    % year with 30 day months. See
    % https://www.mathworks.com/help/fininst/_f5-6010.html#bt17vy8-1 for basis
    % codes if you want to set it to something different 
    % assetPrices: array of prices of the underlying, [asset1price asset2price]
    % vols: array of volatilities of the underlying, [vol1 vol2]. Given in
    % decimal form, i.e., if volatility of asset1 is 22%, then vol1 = 0.22
    % divs: array of dividends of the underlying, [div1 div2]. Given in decimal
    % form *annualized*, i.e., if dividend of asset1 is 6% annually, then div1
    % = 0.06
    % strike: price of underlying asset at which the option can be exercised.
    % Assumes that prices have been previously set to the same decade.
    % corrs: array of possible covariances between the asset prices, i.e.,
    % corrs = [-0.5 0.0 0.1 0.5]
    % optSpec: either 'call' or 'put'
    % maxmin: 0 for minimum, 1 for maximum 
    % ~~~RETURNS~~~
    % prices: a 1xlength(corrs) array of option prices. If maxmin was input
    % with a value other than 0 or 1, returns a 1xlength(corrs) array of NaNs. 

    % defines the interest rate structure
    RateSpec = intenvset('ValuationDate', settle, 'StartDates', settle,...
    'EndDates', maturity, 'Rates', rfr, 'Compounding', -1, 'Basis', basis); 
    % defines the assets on which option is priced 
    AssetSpec1 = stockspec(vols(1), assetPrices(1), 'continuous', divs(1));
    AssetSpec2 = stockspec(vols(2), assetPrices(2), 'continuous', divs(2));
    % prices the option
    if maxmin == 1
        price = maxassetbystulz(RateSpec, AssetSpec1, AssetSpec2,...
        settle, maturity, optSpec, strike, corrs);
    elseif maxmin == 0
        price = minassetbystulz(RateSpec, AssetSpec1, AssetSpec2,...
        settle, maturity, optSpec, strike, corrs);
    else
        price = NaN(length(corrs)); 
    end

end