"""!
Calculates the option prices for the trading simulation.

@author David Dewhurst
"""

from __future__ import print_function
import csv 
import re 
import cPickle 
from glob import glob 
from collections import OrderedDict 
import numpy as np 


def get_matlab_data(TYPES):
	files_from_mlab = glob(TYPES[0]+'*') + glob(TYPES[1]+'*')
	data = {}
	for file in files_from_mlab:
		data[file] = []
		with open(file, 'rb') as f:
			reader = csv.reader(f)
			if re.match(TYPES[0], file):
				data[file].append(['rainbow_option_price'])
			else:
				data[file].append(['vanilla_asset_1_option_price', 
					'vanilla_asset_2_option_price'])
			for line in reader:
				data[file].append(line)
		data[file] = np.asarray(data[file])
	return data 


def get_formatted_data():
	files_from_formatter = glob('r_*.csv')
	data = {}
	for file in files_from_formatter:
		data[file] = []
		with open(file, 'rb') as f:
			reader = csv.reader(f)
			for line in reader:
				data[file].append(line)
		data[file] = np.asarray(data[file])
	return data 


if __name__ == "__main__":

	TYPES = ['RAINBOW', 'VANILLA']
	option_prices = get_matlab_data(TYPES)
	option_prices = OrderedDict(sorted(option_prices.items(), 
		key=lambda t: t[0]))
	formatted_data = get_formatted_data()
	formatted_data = OrderedDict(sorted(formatted_data.items(), 
		key=lambda t: t[0]))

	#print(formatted_data)

	# in order:
	# r_call_high, r_call_low, r_put_high, r_put_low 
	# RAINBOW..., VANILLA...
	call_high = np.column_stack((formatted_data['r_call_high.csv'], 
		option_prices['RAINBOWr_call_high.csv'],
		 option_prices['VANILLAr_call_high.csv']))
	call_low = np.column_stack((formatted_data['r_call_low.csv'], 
		option_prices['RAINBOWr_call_low.csv'],
		 option_prices['VANILLAr_call_low.csv']))
	put_high = np.column_stack((formatted_data['r_put_high.csv'], 
		option_prices['RAINBOWr_put_high.csv'],
		 option_prices['VANILLAr_put_high.csv']))
	put_low = np.column_stack((formatted_data['r_put_low.csv'], 
		option_prices['RAINBOWr_put_low.csv'],
		 option_prices['VANILLAr_put_low.csv']))

	cho = open('ch.pkl', 'wb')
	cPickle.dump(call_high, cho)
	clo = open('cl.pkl', 'wb')
	cPickle.dump(call_low, clo)
	pho = open('ph.pkl', 'wb')
	cPickle.dump(put_high, pho)
	plo = open('pl.pkl', 'wb')
	cPickle.dump(put_low, plo)
