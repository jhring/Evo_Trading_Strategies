function data_2_prices( in_filename, rainbow_out_filename, ...
    vanilla_out_filename )
    % DATA_2_PRICES
    % takes a csv of financial data and writes two csvs, one of rainbow
    % option prices and one of vanilla option prices 
    % ~~~PARAMETERS~~~
    % in_filename: the name of the csv of data to read in
    % rainbow_out_filename: the desired name of the rainbow output file
    % vanilla_out_filename: the desired name of the vanilla output file
    % ~~~RETURNS~~~ 
    % rainbow_out_filename.csv: a single row .csv file of rainbow option prices,
    % each column corresponding to a row of input data in in_filename.csv
    % vanilla_out_filename.csv: a double row .csv file of vanilla option
    % prices. Each column corresponds with an asset, each row with a row of
    % input data in in_filename.csv
    
    % specifies the format of the input data 
    in_data = readtable(in_filename);
    prices = zeros(size(in_data, 1), 1);
    vanilla_prices = zeros(size(in_data, 1), 2);
    
    for i = 1:size(in_data, 1)
        prices(i, 1) = rainbow_option_price(in_data{i, 1}, in_data{i, 2},...
            in_data{i, 3}, in_data{i, 4}, [in_data{i, 5} in_data{i, 6}],...
            [in_data{i, 7} in_data{i, 8}], [in_data{i, 9} in_data{i, 10}],...
            in_data{i,11}, [in_data{i,12}], in_data{i,13},in_data{i,14});
        
        [vanilla_prices(i, 1), vanilla_prices(i, 2)] = vanilla_option_price(in_data{i, 1}, ...
            in_data{i, 2}, in_data{i, 3}, [in_data{i, 5} in_data{i, 6}],...
            [in_data{i, 7} in_data{i, 8}], [in_data{i, 9} in_data{i, 10}],...
            in_data{i,11}, in_data{i,13});
    end
    
    %disp(prices);
    csvwrite(rainbow_out_filename, prices);
    csvwrite(vanilla_out_filename, vanilla_prices);
end