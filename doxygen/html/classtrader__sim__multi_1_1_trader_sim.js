var classtrader__sim__multi_1_1_trader_sim =
[
    [ "__init__", "classtrader__sim__multi_1_1_trader_sim.html#abe73cad32e834033ab9d5ca729f9e215", null ],
    [ "buy_call_high", "classtrader__sim__multi_1_1_trader_sim.html#a0d65eaf6f69e0d588ca03187919c3bf8", null ],
    [ "buy_call_low", "classtrader__sim__multi_1_1_trader_sim.html#afb26d482af85d58a83cde62247e86508", null ],
    [ "buy_put_high", "classtrader__sim__multi_1_1_trader_sim.html#a4f923dc286b283ba61ca2e882f049ec9", null ],
    [ "buy_put_low", "classtrader__sim__multi_1_1_trader_sim.html#aa1c577c7c0da22b474be03cded68eb5b", null ],
    [ "buy_u1", "classtrader__sim__multi_1_1_trader_sim.html#acd6e46eca0c60071edfde16cee415c4d", null ],
    [ "buy_u2", "classtrader__sim__multi_1_1_trader_sim.html#ab8ae5e7cfc28a130a2c7991750e414b9", null ],
    [ "get_call_high", "classtrader__sim__multi_1_1_trader_sim.html#ad2f9ad9e3e15d28fc6180d746c20698d", null ],
    [ "get_call_low", "classtrader__sim__multi_1_1_trader_sim.html#a4b7fc86682ea779d027127de1a57f64d", null ],
    [ "get_put_high", "classtrader__sim__multi_1_1_trader_sim.html#ae1f4f8b4337d7df810f2f43129d070d0", null ],
    [ "get_put_low", "classtrader__sim__multi_1_1_trader_sim.html#a9891374aea154bc8523f2cdfe3d36b36", null ],
    [ "get_value", "classtrader__sim__multi_1_1_trader_sim.html#a7ac0843bef5ccdd7c2f4f5c073340a8d", null ],
    [ "holding_call_high", "classtrader__sim__multi_1_1_trader_sim.html#aa937c8aa26403bdb91fbd95acaab7c53", null ],
    [ "holding_call_low", "classtrader__sim__multi_1_1_trader_sim.html#ac5225a8dd77a5a179f50fca3df74c67f", null ],
    [ "holding_put_high", "classtrader__sim__multi_1_1_trader_sim.html#a9af9117e4aa491f977925a97d3551299", null ],
    [ "holding_put_low", "classtrader__sim__multi_1_1_trader_sim.html#abdde24352a7d7214ac6aadc381f89c5b", null ],
    [ "holding_u1", "classtrader__sim__multi_1_1_trader_sim.html#af504fffe521a65d65fc45a94d1da8963", null ],
    [ "holding_u2", "classtrader__sim__multi_1_1_trader_sim.html#a6572cdb51f1557891f71f6446b6bc0b0", null ],
    [ "if_holding_call_high", "classtrader__sim__multi_1_1_trader_sim.html#a9f9279759f152c21f5145c9c8a3b2897", null ],
    [ "if_holding_call_low", "classtrader__sim__multi_1_1_trader_sim.html#a20a1b8e88bc5a87db712d7fa92e02adc", null ],
    [ "if_holding_put_high", "classtrader__sim__multi_1_1_trader_sim.html#ac320eb76a00180e1372627c9936cad37", null ],
    [ "if_holding_put_low", "classtrader__sim__multi_1_1_trader_sim.html#a42ca2a65476154f2f6ef6f12e931b879", null ],
    [ "if_holding_u1", "classtrader__sim__multi_1_1_trader_sim.html#a31d1352387484e06fad1dfc3aee309b9", null ],
    [ "if_holding_u2", "classtrader__sim__multi_1_1_trader_sim.html#a12a9989a8bbeb797e5704420a9cac3f0", null ],
    [ "if_u1_up_day", "classtrader__sim__multi_1_1_trader_sim.html#ae5889e6abbe057ccffc37d8e3adc3074", null ],
    [ "if_u1_up_total", "classtrader__sim__multi_1_1_trader_sim.html#a694339115fe68553d0170540aefe23d9", null ],
    [ "if_u1_up_week", "classtrader__sim__multi_1_1_trader_sim.html#a260a0f354330c2d3dbee0f784d4b2655", null ],
    [ "if_u2_up_day", "classtrader__sim__multi_1_1_trader_sim.html#add8883f322806e70504db9d71a0afd26", null ],
    [ "if_u2_up_total", "classtrader__sim__multi_1_1_trader_sim.html#aec1dc1fde2ba6e0dc699ad418312aa86", null ],
    [ "if_u2_up_week", "classtrader__sim__multi_1_1_trader_sim.html#a04e01a5e49f520470bc1aa9185181a1d", null ],
    [ "if_up_day", "classtrader__sim__multi_1_1_trader_sim.html#a5fc083a09fb13a7349eae0b9c23bdade", null ],
    [ "if_up_total", "classtrader__sim__multi_1_1_trader_sim.html#a24301715c28a83151bea3ab210482980", null ],
    [ "if_up_week", "classtrader__sim__multi_1_1_trader_sim.html#a5172ee1711e6177beab92f3897808bd6", null ],
    [ "load_data", "classtrader__sim__multi_1_1_trader_sim.html#ad6ac4dd5c90626826a543211c8a20a82", null ],
    [ "run", "classtrader__sim__multi_1_1_trader_sim.html#a5e26a9e926b2e0e6b2a8cd5df9eae5cf", null ],
    [ "sell_call_high", "classtrader__sim__multi_1_1_trader_sim.html#abd1482a11a462155d4555f991f6711f6", null ],
    [ "sell_call_low", "classtrader__sim__multi_1_1_trader_sim.html#a9d873c5955ff6d219d06de348cd7e168", null ],
    [ "sell_put_high", "classtrader__sim__multi_1_1_trader_sim.html#ab763ff7bc87d0fd0ed6421a289bae5e4", null ],
    [ "sell_put_low", "classtrader__sim__multi_1_1_trader_sim.html#ab58bf3df083136dfc821e75df703e7c9", null ],
    [ "sell_u1", "classtrader__sim__multi_1_1_trader_sim.html#a7b854e5ed62c2748d687cca3e0b4f9cf", null ],
    [ "sell_u2", "classtrader__sim__multi_1_1_trader_sim.html#a232fb0cea6e63bba53c8d907898cb927", null ],
    [ "u1_up_day", "classtrader__sim__multi_1_1_trader_sim.html#a50dcfc149a6e8a9a078e63da2e26d415", null ],
    [ "u1_up_total", "classtrader__sim__multi_1_1_trader_sim.html#a7d6801cf718604dd9180b849391575e2", null ],
    [ "u1_up_week", "classtrader__sim__multi_1_1_trader_sim.html#a2135fb377a435cde994860e001289fcd", null ],
    [ "u2_up_day", "classtrader__sim__multi_1_1_trader_sim.html#a939e95030e180a9d73a26688c863bdad", null ],
    [ "u2_up_total", "classtrader__sim__multi_1_1_trader_sim.html#a87bda07c4583d2037aa7f96b6130805a", null ],
    [ "u2_up_week", "classtrader__sim__multi_1_1_trader_sim.html#a63dd9f49bd78e00e2ae98f675e1e2676", null ],
    [ "up_day", "classtrader__sim__multi_1_1_trader_sim.html#a86600339bcdd35204c5496b049d53ce1", null ],
    [ "up_total", "classtrader__sim__multi_1_1_trader_sim.html#ac93c52f924a5982b2c4e265368f48dfb", null ],
    [ "up_week", "classtrader__sim__multi_1_1_trader_sim.html#a892bb81068a10d7787b2890e46e61730", null ],
    [ "algo", "classtrader__sim__multi_1_1_trader_sim.html#a2fb5eab09c2690d5ba76cc3db081255a", null ],
    [ "cash", "classtrader__sim__multi_1_1_trader_sim.html#af412bfdcd71265f0308a43857b8af3c9", null ],
    [ "CH", "classtrader__sim__multi_1_1_trader_sim.html#a3c11c77c6c8cc63be7375005139245ae", null ],
    [ "CL", "classtrader__sim__multi_1_1_trader_sim.html#ac949b528021325c382281264b5bd0aaf", null ],
    [ "fee", "classtrader__sim__multi_1_1_trader_sim.html#a1273df66e3f2b04e2539dfef52a33c35", null ],
    [ "holdings", "classtrader__sim__multi_1_1_trader_sim.html#a29ea20bf15cb4d243d54c2092ed471f0", null ],
    [ "max_steps", "classtrader__sim__multi_1_1_trader_sim.html#af12346fa813cf0a717c1e207c7447a2a", null ],
    [ "PH", "classtrader__sim__multi_1_1_trader_sim.html#a601b31925e1b68f9aab45bb2ae26ea20", null ],
    [ "PL", "classtrader__sim__multi_1_1_trader_sim.html#ad3c3b0db42009bf71a22a0962cba4895", null ],
    [ "start_cash", "classtrader__sim__multi_1_1_trader_sim.html#ace183bf740b536173d5dadbafb38fb71", null ],
    [ "steps", "classtrader__sim__multi_1_1_trader_sim.html#a76a16de98f5edf5f654b4b3ee93589f1", null ],
    [ "U1", "classtrader__sim__multi_1_1_trader_sim.html#a575d746f13957c8dfeb1a388a633cc4e", null ],
    [ "U2", "classtrader__sim__multi_1_1_trader_sim.html#a5ea8eabcb95899947d4e2e2d562d85cc", null ],
    [ "values", "classtrader__sim__multi_1_1_trader_sim.html#a04c017e76698c4b6d2b34cefb9ede141", null ]
];