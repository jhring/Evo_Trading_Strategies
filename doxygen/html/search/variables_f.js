var searchData=
[
  ['path',['path',['../namespaceex__runner.html#a328c1effbf1bc2a8f908e979aa3f7158',1,'ex_runner']]],
  ['ph',['PH',['../classtrader__sim__multi_1_1_trader_sim.html#a601b31925e1b68f9aab45bb2ae26ea20',1,'trader_sim_multi.TraderSim.PH()'],['../classtrader__sim__single_1_1_trader_sim.html#a77926e87ff1b6d385f6c208f90136b33',1,'trader_sim_single.TraderSim.PH()'],['../namespacetrader__sim__multi.html#a0dc17c09f2e8655586cf9fad4fc1dc7f',1,'trader_sim_multi.PH()'],['../namespacetrader__sim__single.html#a6e624fbf26de81c04cdcd791aefa8089',1,'trader_sim_single.PH()']]],
  ['pho',['pho',['../namespacedata__2__trader.html#a883d3d20fdecea7c0e7e57578f3baa5f',1,'data_2_trader']]],
  ['pl',['PL',['../classtrader__sim__multi_1_1_trader_sim.html#ad3c3b0db42009bf71a22a0962cba4895',1,'trader_sim_multi.TraderSim.PL()'],['../classtrader__sim__single_1_1_trader_sim.html#a9ef8d9f93489ee54cd153f24c80b5634',1,'trader_sim_single.TraderSim.PL()'],['../namespacetrader__sim__multi.html#a75e98ee15947737f208c8476bdc5e449',1,'trader_sim_multi.PL()'],['../namespacetrader__sim__single.html#a04ecbdca689cf86a40b4f5400575aa6e',1,'trader_sim_single.PL()']]],
  ['plo',['plo',['../namespacedata__2__trader.html#a328e1a34f16bd4fdc9f0cf4711ba637f',1,'data_2_trader']]],
  ['pop',['pop',['../namespacetrader__sim__multi.html#a42338dcbeded6d135017d28df7389594',1,'trader_sim_multi.pop()'],['../namespacetrader__sim__single.html#a1bd9ad752ca53a1c1f602ac44b9391ec',1,'trader_sim_single.pop()']]],
  ['pre',['pre',['../namespacegeneralization__plots.html#a192c9806494986feb00a7ca22591e43f',1,'generalization_plots']]],
  ['price',['price',['../rainbow__option__price_8m.html#abad7ff40901e61a5ba80e0b9fbb86c66',1,'rainbow_option_price.m']]],
  ['price_5f1',['price_1',['../vanilla__option__price_8m.html#accc80028d7a268242b2160b9e3b76cc2',1,'vanilla_option_price.m']]],
  ['price_5f2',['price_2',['../vanilla__option__price_8m.html#ae807c2cf18a6b9b0a911ed08f094760e',1,'vanilla_option_price.m']]],
  ['prices',['prices',['../data__2__prices_8m.html#a34a72cd9aeb8335701eea3b171b8d153',1,'data_2_prices.m']]],
  ['primitivetree',['PrimitiveTree',['../namespacetrader__sim__multi.html#a94a41128ef328decf9017b1526e09a20',1,'trader_sim_multi.PrimitiveTree()'],['../namespacetrader__sim__single.html#ab710659363e160736f65b0fa55a5dd69',1,'trader_sim_single.PrimitiveTree()']]],
  ['prims_5f1',['prims_1',['../namespaceresults__processing.html#ad9d56dcff031a13b1dbaa02ba0d829d6',1,'results_processing']]],
  ['prims_5f2',['prims_2',['../namespaceresults__processing.html#addfe1544b58de027bf1c5f8920f145eb',1,'results_processing']]],
  ['prims_5f3',['prims_3',['../namespaceresults__processing.html#a044a60843e868fab547dc7503a6d8aa4',1,'results_processing']]],
  ['prims_5f4',['prims_4',['../namespaceresults__processing.html#a2546a07bd47189700d26c882f20fd1af',1,'results_processing']]],
  ['prob_5fprims_5f1',['prob_prims_1',['../namespaceresults__processing.html#ac608e6c0f4e5aca42255ac927dad597b',1,'results_processing']]],
  ['prob_5fprims_5f2',['prob_prims_2',['../namespaceresults__processing.html#abf03559c1cc5fe19cb912130e413ec40',1,'results_processing']]],
  ['prob_5fprims_5f3',['prob_prims_3',['../namespaceresults__processing.html#a569604d475124f00dd38beb5f789fec6',1,'results_processing']]],
  ['prob_5fprims_5f4',['prob_prims_4',['../namespaceresults__processing.html#a6b4e6b94b3bf3c4a202a305cb0c71681',1,'results_processing']]],
  ['probs_5f1',['probs_1',['../namespaceresults__processing.html#ad0ea658629a34d5865e1aeaca929e100',1,'results_processing']]],
  ['probs_5f2',['probs_2',['../namespaceresults__processing.html#a82850825bb237c8062be7a2b7934eb9f',1,'results_processing']]],
  ['probs_5f3',['probs_3',['../namespaceresults__processing.html#a55f0977e137162624ccd84236dec4a9a',1,'results_processing']]],
  ['probs_5f4',['probs_4',['../namespaceresults__processing.html#a26d9f815bf77db103c7519eee78b224e',1,'results_processing']]],
  ['profit',['profit',['../namespacetrader__sim__multi.html#a6b1ea5faeb6eae5cb9dc96f82d7f192f',1,'trader_sim_multi.profit()'],['../namespacetrader__sim__single.html#a8b878c3319d3ff80023d0f9829a73462',1,'trader_sim_single.profit()']]],
  ['profits',['profits',['../namespacegeneralization__plots.html#a92a0f6f18a2ff26899e148d559c1246a',1,'generalization_plots']]],
  ['pset',['pset',['../namespacetrader__sim__multi.html#ad6b2b96c97343a67af9d89bc62b46d12',1,'trader_sim_multi.pset()'],['../namespacetrader__sim__single.html#aa2f0403f0f6cd3138042a337586f9f1f',1,'trader_sim_single.pset()']]],
  ['put_5fhigh',['put_high',['../namespacedata__2__trader.html#aa51cb1f54dbdf78d903dba81dabfd9fd',1,'data_2_trader']]],
  ['put_5flow',['put_low',['../namespacedata__2__trader.html#a4e54ab6bc0e6218decc8386cb24f8608',1,'data_2_trader']]],
  ['pval_5f1',['pval_1',['../namespaceresults__processing.html#ab61ac7c18c33ea30c9884518efac92f9',1,'results_processing']]],
  ['pval_5f2',['pval_2',['../namespaceresults__processing.html#a71d5acff337c421fdb8f3cd101d80f64',1,'results_processing']]],
  ['pval_5f3',['pval_3',['../namespaceresults__processing.html#a336c8d4d8ba6e339c217d3309294e28b',1,'results_processing']]],
  ['pval_5f4',['pval_4',['../namespaceresults__processing.html#acdcb2c32511bc3c971b28107a7eb71ca',1,'results_processing']]],
  ['pval_5fdiff_5fprims',['pval_diff_prims',['../namespaceresults__processing.html#a1318000d093a8d607042c17dbd7a1888',1,'results_processing']]],
  ['pval_5fdiff_5fprims2',['pval_diff_prims2',['../namespaceresults__processing.html#ad633f4ba8bd5f7353ed202356e2c03c0',1,'results_processing']]],
  ['pval_5fterm_5f1',['pval_term_1',['../namespaceresults__processing.html#a77b38739815ea67d1a4a0c2ed65ea4ef',1,'results_processing']]],
  ['pval_5fterm_5f2',['pval_term_2',['../namespaceresults__processing.html#acc2d66220f68856ca4b7abebdd2a2d3d',1,'results_processing']]],
  ['pval_5fterm_5f3',['pval_term_3',['../namespaceresults__processing.html#a43f0a3313469cb9f67c32b88514f1f5f',1,'results_processing']]],
  ['pval_5fterm_5f4',['pval_term_4',['../namespaceresults__processing.html#a90255f78291b0f7bcf29be7509850c64',1,'results_processing']]]
];
