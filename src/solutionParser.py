import re
import pickle

def findEndIndex(string):
	pCount = 0
	for i in range(len(string)):
		if string[i] is '(':
			pCount += 1

		elif string[i] is ')':
			pCount -= 1
			
			if pCount is 0:
				return i
	return -1


def getProgArgs(strFunc):
	# throw away the prog and its parenthesis
	strFunc = strFunc[6:-1]

	# split the remaining string
	openPar = 1
	last=0
	args=[]
	solution = list(strFunc)
	for i in range(len(strFunc)):
		c = solution[i]
		# Count parenthesis to know when to split the string
		if c is "(":
			openPar += 1

		elif c is ")":
			openPar -= 1

		# if parenthesis are balanced then we must be between
		# arguments, so grab the argument then keep iterating
		elif c is ',' and openPar == 1:
			args.append(''.join(strFunc[last:i]))
			last = i+2

	# Grabs the last argument, which is skipped in the loop above
	args.append(''.join(strFunc[last:]))

	return args


def formatIfBlock(strIf, tabs=0):
	tabCount = tabs + 1
	pCount = 1
	strIf = list('\t'*tabs + strIf)
	strIf[strIf.index('_')] = ' '
	start = strIf.index('(')
	strIf[start] = ':\n' + '\t'*tabCount

	for i in range(start, len(strIf)):
		c = strIf[i]

		if c is '(':
			pCount += 1

		elif c is ')':
			pCount -= 1

		elif c is ',' and pCount is 1:
			strIf[i] = '\n' + '\t'*tabs + 'else:\n' + '\t'*tabCount

	strIf = ''.join(strIf)

	if strIf[-1] is ')':
		strIf = strIf[:-1]

	strIf = strIf.replace('\t ', '\t')

	return strIf


def countSubstring(string, substring, returnType=False):
	index = 0
	count = 0
	indexList = []

	while string.find(substring, index) is not -1:
		index = string.find(substring, index)+1
		count += 1
		indexList.append(index-1)

	if returnType:
		return indexList

	else:
		return count


def getTabCount(solution, start):
	tabCount = 0
	index = start
	prevChars = solution[index-1:index]
	while prevChars == '\t':
		tabCount += 1
		index -= 1
		prevChars = solution[index-1:index]

	return tabCount


def strategySimplifier(solution):
	# Format all if blocks
	while re.search(r'if_.*?\(.*\)', solution):
		if re.match(r'if_.*?\(.*\)', solution):
			solution = formatIfBlock(solution)

		else:
			found = re.search(r'if_.*?\(.*?\)', solution)
			start = found.start()
			end = start+findEndIndex(solution[start:])+1
			new = formatIfBlock(solution[start:end], getTabCount(solution, start))
			solution = solution[:start] + '\n' + new +solution[end:]
	
	# Remove and format all prog terms
	while re.search(r'prog[234].*', solution):
		# if the outer most call is a prog, pass directly to the argument retriever
		if re.match(r'prog[234]\(.*\)', solution):
			new = getProgArgs(solution)
			solution = '\n'.join(new)

		# otherwise find the next prog and format it
		else:
			found = re.search(r'prog[234].*', solution)
			start = found.start()
			end = start + findEndIndex(solution[start:])+1
			new = getProgArgs(solution[start:end])
			tabCount = getTabCount(solution, start)
			solution = solution[:start] + ('\n'+'\t'*tabCount).join(new) + solution[end:]

	return solution


if __name__ == '__main__':
	# solution = 'prog4(if_holding_call_high(prog2(buy_u1, sell_u2), if_holding_call_low(sell_u2, buy_u2)), prog2(buy_ch, buy_pl), prog2(sell_ph, buy_cl), prog2(sell_u1, buy_u2))'

	#print(getTabCount('\t\t\t\n\t\t\t\tprog3(a, b, c)', 8))
	
	with open('../results/ex2_multigeneralization_result.pkl', 'rb') as f:
		data = pickle.load(f)

	solution = data.values()[30][0][2]

	print(solution + '\n')

	parsed = strategySimplifier(solution)

	print"*****************************************************************************************"
	print(parsed)