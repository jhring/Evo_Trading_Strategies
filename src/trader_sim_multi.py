"""!
Implements a trading simulator class which allows for the evaluation of trading
strategies in a multiple action per time step scheme. Strategies are evolved
using AFPO.

@author John H. Ring IV
@author Colin M. Van Oort
"""


from deap import base, creator, gp, tools
from functools import partial
import numpy as np
import operator
import pandas as pd

from dataset_builder import build_dataset
from mapping_fix import _map
import afpo


def main():
    data = build_dataset()

    pop, logbook, hof = evolve_strategies(data,
                                          initial_capital=10000,
                                          trading_fee=14,
                                          pop_size=50,
                                          generations=25,
                                          demo=True)

    print('Stocks:               {}'.format(list(data.columns)))
    print('Trading Days:         {}'.format(len(data.index)))
    print('Date Range:           {} to {}'.format(data.index[0], data.index[-1]))
    print('Best fitness:         {}'.format(logbook.chapters['fitness'].select('max')[-1]))
    print('Average Annual Gains: {}'.format(logbook.chapters['fitness'].select('max')[-1] / (len(data.index) / 252.)))


def evolve_strategies(market_data,
                      initial_capital=10000,
                      trading_fee=7,
                      pop_size=50,
                      generations=25,
                      demo=False):
    """!
    Evolves a population of trading strategies using A.F.P.O.

    @return (list) Evolved population, log of evolution dynamics, hall of fame
    """
    trader = Trader(initial_capital, trading_fee, market_data)

    toolbox = build_gp_toolbox(trader)

    hof = tools.HallOfFame(5)

    pop, logbook = afpo.afpo(toolbox.population(n=pop_size),
                             toolbox,
                             0.5,
                             0.2,
                             generations,
                             5,
                             build_stats(),
                             hall_of_fame=hof,
                             verbose=False,
                             demo=demo)

    return pop, logbook, hof


class Trader():
    """!
    Simulates a trader operating on a financial market.
    The trader maintains information about the state of the market,
    has the ability to interact with the market in several ways,
    and can execute trading stategies.
    """
    def __init__(self, cash, fee, market_data):
        """!
        @param cash (float) Initial capital.
        @param fee (float) Applied per trade.
        @param market_data (Numpy.DataFrame) Prices of financial instruments.
        """
        self.stocks = list(market_data.columns)
        self.market_data = market_data.values.T
        self.start_cash = cash
        self.max_steps = len(market_data.index) - 1
        self.fee = fee

        self.reset()

        self.build_trader_primitives()

    def reset(self):
        """!
        Prepares the trader for a new trading period by restoring default
        values to several instance variables.
        """
        self.steps = 0
        self.cash = self.start_cash
        self.holdings = {key: 0 for key in range(len(self.stocks))}
        self.values = np.zeros(self.max_steps + 1)
        self.values[0] = self.cash

        self.buy_log = pd.DataFrame(np.zeros((self.max_steps, len(self.stocks)), dtype=int), columns=self.stocks)

        self.sell_log = pd.DataFrame(np.zeros((self.max_steps, len(self.stocks)), dtype=int), columns=self.stocks)

    def build_trader_primitives(self):
        self.terminals = []
        self.primitives = []

        primitive_helpers = []

        # Primitives and terminals related to each financial instrument
        for index, name in enumerate(self.stocks):
            setattr(self,
                    'buy_{}'.format(name),
                    partial(self.buy, index, 1))
            self.terminals.append('buy_{}'.format(name))

            setattr(self,
                    'bulk_buy_{}'.format(name),
                    partial(self.buy, index, 100))
            self.terminals.append('bulk_buy_{}'.format(name))

            setattr(self,
                    'sell_{}'.format(name),
                    partial(self.sell, index, 1))
            self.terminals.append('sell_{}'.format(name))

            setattr(self,
                    'bulk_sell_{}'.format(name),
                    partial(self.sell, index, 100))
            self.terminals.append('bulk_sell_{}'.format(name))

            setattr(self,
                    '{}_up_total'.format(name),
                    partial(self.check_total_difference, index))
            primitive_helpers.append('{}_up_total'.format(name))

            setattr(self,
                    '{}_up_week'.format(name),
                    partial(self.check_positive_difference, index, 5))
            primitive_helpers.append('{}_up_week'.format(name))

            setattr(self,
                    '{}_up_day'.format(name),
                    partial(self.check_positive_difference, index, 1))
            primitive_helpers.append('{}_up_day'.format(name))

            setattr(self,
                    'holding_{}'.format(name),
                    partial(self.check_holdings, index))
            primitive_helpers.append('holding_{}'.format(name))

        # Trader status primitives
        primitive_helpers.append(self.trader_up_day.__name__)
        primitive_helpers.append(self.trader_up_week.__name__)
        primitive_helpers.append(self.trader_up_total.__name__)

        # Conditional primitives using existing boolean primitives
        for helper in primitive_helpers:
            setattr(self,
                    'if_' + helper,
                    partial(self.if_then_else, getattr(self, helper)))
            self.primitives.append('if_' + helper)

    def buy(self, instrument, count):
        price = count * self.market_data[instrument, self.steps]

        if self.cash >= price:
            self.cash -= price
            self.holdings[instrument] += count
            self.buy_log.iloc[self.steps, instrument] += count

    def sell(self, instrument, count):
        price = count * self.market_data[instrument, self.steps]

        if self.holdings[instrument] >= count:
            self.holdings[instrument] -= count
            self.cash += price
            self.sell_log.iloc[self.steps, instrument] += count

    def check_total_difference(self, instrument):
        return self.market_data[instrument, self.steps] > self.market_data[instrument, 0]

    def check_positive_difference(self, instrument, delta):
        try:
            return self.market_data[instrument, self.steps] > self.market_data[instrument, self.steps - delta]

        except KeyError:
            return False

    def check_holdings(self, instrument):
        return self.holdings[instrument] > 0

    def trader_up_day(self):
        if self.steps >= 1:
            return self.get_value() > self.values[self.steps - 1]
        else:
            return False

    def trader_up_week(self):
        if self.steps >= 5:
            return self.get_value() > self.values[self.steps - 5]
        else:
            return False

    def trader_up_total(self):
        return self.get_value() > self.values[0]

    def get_value(self):
        value = self.cash

        for instrument in range(len(self.stocks)):
            value += self.holdings[instrument] * self.market_data[instrument, self.steps]

        return value

    def if_then_else(self, condition, function1, function2):
        return function1 if condition() else function2

    def get_primitive_set(self):
        self.pset = gp.PrimitiveSet("MAIN", 0)

        for terminal in self.terminals:
            self.pset.addTerminal(getattr(self, terminal), name=terminal)

        for primitive in self.primitives:
            self.pset.addPrimitive(getattr(self, primitive), 2, name=primitive)

        self.pset.addPrimitive(self.prog2, 2)
        self.pset.addPrimitive(self.prog3, 3)
        self.pset.addPrimitive(self.prog4, 4)

        return self.pset

    def progn(self, *args):
        for arg in args:
            arg()

    def prog2(self, function1, function2):
        return partial(self.progn, function1, function2)

    def prog3(self, function1, function2, function3):
        return partial(self.progn, function1, function2, function3)

    def prog4(self, function1, function2, function3, function4):
        return partial(self.progn, function1, function2, function3, function4)

    def evaluate_strategy(self, individual):
        """!
        Evaluates a trading strategy under a fixed set of market conditions.

        @param individual The individual to evaluate.

        @return (list) Profits and age
        """
        # Transform the tree expression into functional Python code
        algo = gp.compile(individual, self.pset)

        self.run(algo)

        profit = (self.values[self.max_steps] - self.values[0]) / self.values[0] * 100.

        return (profit, individual.age,)

    def test_strategy(self, individual):
        """!
        A fitness function for testing previous generated solutions.
        """
        # Transform the tree expression into functional Python code
        profit, age = self.evaluate_strategy(individual)

        return profit

    def run(self, algo):
        """!
        @param algo Compiled trading strategy.
        """
        self.reset()

        while self.steps < self.max_steps:
            algo()
            self.values[self.steps + 1] = self.get_value() - self.fee
            self.steps += 1


def build_gp_toolbox(trader):
    toolbox = base.Toolbox()

    creator.create("FitnessMulti",
                   base.Fitness,
                   weights=(1.0, -1.0,))

    pset = trader.get_primitive_set()

    creator.create("Individual",
                   gp.PrimitiveTree,
                   fitness=creator.FitnessMulti,
                   pset=pset,
                   age=1)

    # Expression tree generator
    toolbox.register("initializer",
                     gp.genHalfAndHalf,
                     pset=pset,
                     min_=1,
                     max_=2)

    toolbox.register("individual",
                     tools.initIterate,
                     creator.Individual,
                     toolbox.initializer)

    toolbox.register("population",
                     tools.initRepeat,
                     list,
                     toolbox.individual)

    toolbox.register("evaluate", trader.evaluate_strategy)

    toolbox.register("select",
                     afpo.tournament_selection,
                     tournsize=3)

    toolbox.register("mate", afpo.one_point_crossover)

    toolbox.register("expr_mut",
                     gp.genHalfAndHalf,
                     min_=0,
                     max_=2)

    toolbox.register("mutate",
                     gp.mutUniform,
                     expr=toolbox.expr_mut,
                     pset=pset)

    toolbox.decorate("mate", gp.staticLimit(key=operator.attrgetter("height"), max_value=17))
    toolbox.decorate("mutate", gp.staticLimit(key=operator.attrgetter("height"), max_value=17))

    # Multiprocessing using scoop
    toolbox.register("map", _map)

    return toolbox


def build_stats():
    stats_fit = tools.Statistics(lambda ind: ind.fitness.values[0])
    stats_age = tools.Statistics(lambda ind: ind.fitness.values[1])
    stats_size = tools.Statistics(lambda ind: ind.height)

    stats = tools.MultiStatistics(fitness=stats_fit,
                                  age=stats_age,
                                  size=stats_size)

    stats.register("min", np.min)
    stats.register("max", np.max)
    stats.register("avg", np.mean)

    return stats


if __name__ == '__main__':
    main()
