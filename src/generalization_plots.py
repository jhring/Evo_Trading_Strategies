"""!
Generates plots of summary statistics and observed distributions of the profitability
of individual solutions when evaluated on out of training sample data.

@author David Dewhurst
@author John H. Ring IV
"""
import pickle
import numpy as np
import matplotlib.pyplot as plt
import os
from statsmodels.nonparametric.kde import KDEUnivariate
from mpl_toolkits.axes_grid.inset_locator import inset_axes
import matplotlib.ticker as mtick
from matplotlib.ticker import MaxNLocator


def get_profits(resultStr):
    with open(resultStr, 'rb') as f:
        result = pickle.load(f)

    dim1 = len(result.keys())
    dim2 = len(result.values()[1])

    profits = np.zeros((dim1, dim2))

    keys = result.keys()

    for i in range(len(keys)):
        key = keys[i]
        for j in range(len(result[key])):
            profits[i, j] = result[key][j][1]

    return profits, keys


def make_plots(profits, keys, path):
    if not os.path.exists(path):
        os.makedirs(path)

    profits = np.sort(profits, axis=1)

    for i in range(len(keys)):
        fig = plt.figure(figsize=(18, 5))

        # plot the empirical CDF
        ax = fig.add_subplot(1, 2, 1)
        LABEL = keys[i]

        s = np.sort(profits[i, :])
        y = np.arange(1, len(s) + 1) / float(len(s))
        cdf, = ax.plot(profits[i, :], y, 'k-', linewidth=2)

        ax.xaxis.set_major_formatter(mtick.FormatStrFormatter('%1.e'))
        ax.set_xlabel('Profit,nominal dollars')
        ax.set_ylabel('Probability')

        # plot the empirical pdf and kde estimation of true pdf
        est_pdf = KDEUnivariate(profits[i, :])
        est_pdf.fit()

        ax2 = fig.add_subplot(1, 2, 2)
        ax2.hist(profits[i, :], bins=np.sqrt(len(profits[i, :])) + 2, normed=True, color='red')

        kde_est, = ax2.plot(est_pdf.support, est_pdf.density, lw=2, color='black', label='$P_{kde}$')

        ax2.set_xlabel('Profit, nominal dollars')
        ax2.set_ylabel('Probability')
        ax2.legend(handles=[kde_est])

        mean = 'Average profit:' + str(round(np.mean(profits[i, :]), 2))
        minimum = 'Minimum profit:' + str(round(np.min(profits[i, :]), 2))
        maximum = 'Maximum profit:' + str(round(np.max(profits[i, :]), 2))

        ax2.text(0.99,
                 0.6,
                 minimum,
                 verticalalignment='bottom',
                 horizontalalignment='right',
                 transform=ax2.transAxes,
                 fontsize=14)

        ax2.xaxis.set_major_formatter(mtick.FormatStrFormatter('%1.e'))

        ax2.text(0.99,
                 0.5,
                 mean,
                 verticalalignment='bottom',
                 horizontalalignment='right',
                 transform=ax2.transAxes,
                 fontsize=14)

        ax2.xaxis.set_major_formatter(mtick.FormatStrFormatter('%1.e'))

        ax2.text(0.99,
                 0.4,
                 maximum,
                 verticalalignment='bottom',
                 horizontalalignment='right',
                 transform=ax2.transAxes,
                 fontsize=14)

        ax2.xaxis.set_major_formatter(mtick.FormatStrFormatter('%1.e'))

        # now do log-log plot
        ax3 = inset_axes(ax,
                         width="25%",  # width = 25% of parent_bbox
                         height=1.5,  # height = 1.5 inches
                         loc=7)

        log_profits = np.log10(est_pdf.support)
        llplot, = ax3.plot(log_profits, est_pdf.density, 'k-', linewidth=2)

        ax3.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax3.set_xlabel('$\log_{10} \pi$')
        ax3.set_ylabel('$p(\pi)$')

        ax.set_title(LABEL)

        plt.tight_layout()
        plt.savefig(path + keys[i] + '.png')

        plt.close()

if __name__ == "__main__":
    pre = '../results/generalization_plots/'
    profits, keys = get_profits('../results/ex1_multigeneralization_result.pkl')
    make_plots(profits, keys, pre + 'ex1_multi/')

    profits, keys = get_profits('../results/ex2_multigeneralization_result.pkl')
    make_plots(profits, keys, pre + 'ex2_multi/')

    profits, keys = get_profits('../results/ex1_singlegeneralization_result.pkl')
    make_plots(profits, keys, pre + 'ex1_single/')

    profits, keys = get_profits('../results/ex2_singlegeneralization_result.pkl')
    make_plots(profits, keys, pre + 'ex2_single/')
