"""
A simple script to batch run experiments.

@author John H. Ring IV
"""
import subprocess

subprocess.call('python ex_runner.py 1 single', shell=True)
subprocess.call('python ex_runner.py 2 single', shell=True)
subprocess.call('python ex_runner.py 1 multi', shell=True)
subprocess.call('python ex_runner.py 2 multi', shell=True)
