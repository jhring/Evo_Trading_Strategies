"""! @package ex_runner
A simple script to run the trading simulation on many combinations
of dates and securities and then evaluate them on all possible data sets.

This script takes two command line arguments ex and ver.
When ex=1 the simulation runs without the ability to trade the underlying securities
for ex=2 this restrict is lifted. ver may be set to single or multi when ver is single
the algorithm may only preform one action per day. When ver is multi this restriction is lifted. 

@author John H. Ring IV
@author David Dewhurst
"""

from __future__ import print_function 
import subprocess 
import os 
import sys
import cPickle as pickle
from itertools import product 
import random


if __name__ == "__main__":

    ex = '2' #experiment 1 or 2
    ver = 'multi' #single or multi

    DATES = [['19980105', '19980506', '19990105'], 
    ['19980506', '19981106', '19990506'], 
    ['19990105', '19990506', '20000105'], 
    ['19990506', '19991106', '20000506'], 
    ['20000105', '20000506', '20010105'],
    ['20000506', '20001106', '20010506'],
    ['20010105', '20010506', '20020105'],
    ['20010506', '20011106', '20020506'],
    ['20020105', '20020506', '20030105'], 
    ['20020506', '20021106', '20030506'], 
    ['20030105', '20030506', '20040105'],
    ['20030506', '20031106', '20040506']]
    SECURITIES = [['aapl', 't'], ['ibm', 'xom'], ['msft', 'x']]

    conditions = [_ for _ in product(DATES, SECURITIES)]
    while True:
        date, sec = random.choice(conditions)
        d = '../data/processed/'
        n = date[0]+'-'+date[1]+'-'+date[2]+sec[0]+'-'+sec[1]+'.pkl'
        subprocess.call('python -m scoop trader_sim_'+ver+'.py ' +d+'ch_'+n + ' ' + d + 'cl_' + n + ' ' + d + 'ph_' + n +' ' + d + 'pl_' + n + ' ' + ex, shell=True)

